import * as actionTypes from '../../types/TimerTypes';
const initialState = {
  id:0,
  sessionIndex: 0,
  sessionEnded: false,
  sessionRests: 0,
  sessionSkipped:0,
  sessions: [
    { 
      id:0,
      rounds: 1,
      rests: 0,
      skips: 0,
    },
  ],
  min:0,
  time: {
    min: 0,
    sec: 1,
  },
};
const sessionReducers = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TIMER_TRAIN:      
        return {
          ...state,
          sessions: update(state,"rounds"),
        };
      
    case actionTypes.TIMER_REST_ROUND:
      return {
        ...state,
        sessions: update(state,"rests"),
        time: {min: 10, sec: 0},
      };
    case actionTypes.TIMER_REST_SESSION:
      return{
        ...state,
        sessionRests:state.sessionRests++,
        time:{min:25,sec:0}
      }
    case actionTypes.TIMER_Next_ROUND:
      // let obj = {rounds:state.sessions[sessionIndex].rounds+=1}
      return {
        ...state,
        id:state.id+1,
        time:{min:0,sec:1},
        sessions: update(state,"rounds")
      };
      case actionTypes.TIMIER_Next_SESSION:
        const newId = state.sessions[state.sessionIndex].id++
        let newIndex = state.sessionIndex+=1
        let newSession = [...state.sessions,{rounds: 1, rests: 0, skips: 0,id:newId}]
        let timeObj = {min:25 ,sec:0}
        return {
          ...state,
          id:state.id+1,
          sessionEnded:true,
          time: Object.assign({min:25,sec:0},state.time),
          sessionIndex: newIndex,
          sessions: newSession,

        };
      case actionTypes.ADD_DATA:
        return {
          ...state,
          sessions:action.payload
        }
      case actionTypes.TIMER_RESTART_SESSION:
        return{
          ...state,
          sessionEnded:false
        }
      default:
      return {
        ...state,
      };
  }
};
let update = (state,value) =>{
let newOb = state.sessions
let tempObj = newOb[state.sessionIndex]

tempObj[value]+=1
newOb[state.sessionIndex] = tempObj
return newOb
}
export default sessionReducers;
